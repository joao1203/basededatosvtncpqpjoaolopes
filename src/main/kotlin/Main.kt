import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import nl.adaptivity.xmlutil.serialization.XML
import java.io.*
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import kotlin.io.path.Path
import kotlin.io.path.writeText

@Serializable
data class Restaurant(val address: Address,
                      val borough: String,
                      val cuisine: String,
                      val grades: List<Grades>,
                      val name: String,
                      val restaurant_id: String): java.io.Serializable

@Serializable
data class Address(val building: String,
                   val coord: List<Double>,
                   val street: String,
                   val zipcode: String): java.io.Serializable

@Serializable
data class Grades(val date: String,
                  val mark: String,
                  val score: Int): java.io.Serializable

val jsonFile = File("src/main/kotlin/restaurants.json")
val datFile = File("src/main/kotlin/restaurants.dat")
val objectList = mutableListOf<Restaurant>()

fun main() {
    exercici1()
    exercici2()
    exercici3()
}

fun exercici1() {
    val readJson = jsonFile.readLines()

    for (i in readJson){
        val obj = Json.decodeFromString<Restaurant>(i)
        objectList.add(obj)
    }
}

fun exercici2() {
    // Codify Objects to Binary File
    val fileOutput = FileOutputStream(datFile)
    val fileOutputObject = ObjectOutputStream(fileOutput)
    for (i in objectList.indices){
        fileOutputObject.writeObject(objectList[i])
    }
    // Decodify Binary File to Objects
    val fileInput = ObjectInputStream(FileInputStream(datFile))
    for (i in 1..objectList.size){
        println(fileInput.readObject())
    }
}
fun exercici3(){
    val readDat = datFile.readLines()
    for (i in readDat){
        val obj = Json.decodeFromString<Restaurant>(i)
        val xmlObj = XML.encodeToString(obj)
        val cringe = FileOutputStream("src/main/kotlin/restaurants.xml", true)
        cringe.bufferedWriter().use { writer ->
            writer.write(xmlObj + "\n")
        }
    }
}